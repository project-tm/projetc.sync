<?

namespace Projetc\Sync\Event;

use CUser,
    Projetc\Sync\Platform\Update;

class User {

    public static function OnSaleOrderSaved($event) {
        $arFiles = $event->getParameters();
        if ($arFiles['IS_NEW']) {
            $collection = $arFiles['ENTITY']->getBasket()->getOrder()->getPropertyCollection();
            $user = new CUser;
            $fields = array();
            if ($phone = $collection->getItemByOrderPropertyId(3)->getValue()) {
                $fields['PERSONAL_PHONE'] = $phone;
            }
            if ($city = $collection->getItemByOrderPropertyId(4)->getValue()) {
                $fields['PERSONAL_CITY'] = $city;
            }
            if ($adress = $collection->getItemByOrderPropertyId(5)->getValue()) {
                $fields['PERSONAL_STREET'] = $adress;
            }
    //        preExit($fields);
            $user->Update(cUser::getId(), $fields);
        }
    }

}
