<?

namespace Projetc\Sync\Platform;

use CDBResult,
    CIBlockPropertyEnum,
    Project\Core\Trains\Event,
    Igromafia\Game\Property,
    Projetc\Sync\Platform\Config;

class Update {

    use Event;

    static protected function evetType() {
        return __CLASS__;
    }

    public static function hl($arFiles) {
        if (self::start()) {
            $class = Property::getHl(Config::HL_CODE);
            foreach ($arFiles['VALUES'] as $value) {
//                pre($value);
                $rsData = $class::getList(array(
                            "select" => array('ID', 'UF_XML_ID', 'UF_NAME'),
                            "filter" => array('UF_NAME' => $value['VALUE']),
                ));
                $rsData = new CDBResult($rsData);
                if ($arItem = $rsData->Fetch()) {
//                    pre($arItem);
                    if ($arItem['UF_XML_ID'] != $value['XML_ID']) {
//                        pre($arItem['ID'], array(
//                            'UF_XML_ID' => $value['XML_ID']
//                        ));
                        $class::update($arItem['ID'], array(
                            'UF_XML_ID' => $value['XML_ID']
                        ));
                    }
                } else {
//                    pre(array(
//                        'UF_XML_ID' => $value['XML_ID'],
//                        'UF_NAME' => $value['VALUE']
//                    ));
                    $class::add(array(
                        'UF_XML_ID' => $value['XML_ID'],
                        'UF_NAME' => $value['VALUE']
                    ));
//                    preExit($arItem);
                }
            }
        }
    }

    public static function deleteProps($ID) {
        if (self::start()) {
            $class = Property::getHl(Config::HL_CODE);
            $rsData = $class::getList(array(
                        "select" => array('UF_NAME'),
                        "filter" => array('ID' => $ID),
            ));
            $rsData = new CDBResult($rsData);
            while ($arItem = $rsData->Fetch()) {
                $arFilter = array(
                    'IBLOCK_ID' => Config::IBLOCK_ID,
                    'PROPERTY_ID' => Config::PROPERTY_ID,
                    'VALUE' => $arItem['UF_NAME']
                );
                $res = CIBlockPropertyEnum::GetList(array(), $arFilter);
                if ($arFields = $res->Fetch()) {
//                    pre($arFields['ID']);
                    CIBlockPropertyEnum::Delete($arFields['ID']);
                }
            }
            self::stop();
        }
    }

    public static function props($name, $xmlId) {
        if (self::start()) {
            $arFilter = array(
                'IBLOCK_ID' => Config::IBLOCK_ID,
                'PROPERTY_ID' => Config::PROPERTY_ID,
                'VALUE' => $name
            );
            $ibpenum = new CIBlockPropertyEnum;
            $res = CIBlockPropertyEnum::GetList(array(), $arFilter);
            if ($arFields = $res->Fetch()) {
                if ($xmlId != $arFields['XML_ID']) {
//                    pre($arFields['ID'], Array(
//                        'XML_ID' => $xmlId
//                    ));
                    $ibpenum->Update($arFields['ID'], Array(
                        'XML_ID' => $xmlId
                    ));
                }
            } else {
//                pre(Array(
//                    'PROPERTY_ID' => Config::PROPERTY_ID,
//                    'XML_ID' => $xmlId,
//                    'VALUE' => $name
//                ));
                $ibpenum->Add(Array(
                    'PROPERTY_ID' => Config::PROPERTY_ID,
                    'XML_ID' => $xmlId,
                    'VALUE' => $name
                ));
            }
            self::stop();
        }
    }

}
