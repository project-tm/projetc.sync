<?php

include (__DIR__ . '/../lib/user.php');

use Bitrix\Main\Application,
    Bitrix\Main\ModuleManager,
    Bitrix\Main\Localization\Loc,
    Bitrix\Main\Loader;

IncludeModuleLangFile(__FILE__);

class projetc_sync extends CModule {

    public $MODULE_ID = 'projetc.sync';

    function __construct() {
        $arModuleVersion = array();

        include(__DIR__ . '/version.php');

        if (is_array($arModuleVersion) && array_key_exists('VERSION', $arModuleVersion)) {
            $this->MODULE_VERSION = $arModuleVersion['VERSION'];
            $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
        }

        $this->MODULE_NAME = Loc::getMessage('PROJETC_SYNC_NAME');
        $this->MODULE_DESCRIPTION = Loc::getMessage('PROJETC_SYNC_DESCRIPTION');
        $this->PARTNER_NAME = Loc::getMessage('PROJETC_SYNC_PARTNER_NAME');
        $this->PARTNER_URI = '';
    }

    public function DoInstall() {
        ModuleManager::registerModule($this->MODULE_ID);
        Loader::includeModule($this->MODULE_ID);
        $this->InstallEvent();
    }

    public function DoUninstall() {
        Loader::includeModule($this->MODULE_ID);
        $this->UnInstallEvent();
        ModuleManager::unRegisterModule($this->MODULE_ID);
    }

    public function InstallEvent() {
        $eventManager = Bitrix\Main\EventManager::getInstance();
        $eventManager->registerEventHandler('', 'PlatformOnAfterAdd', $this->MODULE_ID, '\Projetc\Sync\Event\Platform', 'OnAfterAdd');
        $eventManager->registerEventHandler('', 'PlatformOnAfterUpdate', $this->MODULE_ID, '\Projetc\Sync\Event\Platform', 'OnAfterUpdate');
        $eventManager->registerEventHandler('', 'PlatformOnBeforeDelete', $this->MODULE_ID, '\Projetc\Sync\Event\Platform', 'OnBeforeDelete');

        $eventManager->registerEventHandler('iblock', 'OnAfterIBlockPropertyUpdate', $this->MODULE_ID, '\Projetc\Sync\Event\Platform', 'OnAfterIBlockPropertyUpdate');

        $eventManager->registerEventHandler('sale', 'OnSaleOrderSaved', $this->MODULE_ID, '\Projetc\Sync\Event\User', 'OnSaleOrderSaved');
    }

    public function UnInstallEvent() {
        $eventManager = Bitrix\Main\EventManager::getInstance();
        $eventManager->unRegisterEventHandler('', 'PlatformOnAfterAdd', $this->MODULE_ID, '\Projetc\Sync\Event\Platform', 'OnAfterAdd');
        $eventManager->unRegisterEventHandler('', 'PlatformOnAfterUpdate', $this->MODULE_ID, '\Projetc\Sync\Event\Platform', 'OnAfterUpdate');
        $eventManager->unRegisterEventHandler('', 'PlatformOnBeforeDelete', $this->MODULE_ID, '\Projetc\Sync\Event\Platform', 'OnBeforeDelete');

        $eventManager->unRegisterEventHandler('iblock', 'OnAfterIBlockPropertyUpdate', $this->MODULE_ID, '\Projetc\Sync\Event\Platform', 'OnAfterIBlockPropertyUpdate');

        $eventManager->unRegisterEventHandler('sale', 'OnSaleOrderSaved', $this->MODULE_ID, '\Projetc\Sync\Event\User', 'OnSaleOrderSaved');
    }

}
